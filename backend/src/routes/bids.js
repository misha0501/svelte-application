const express = require('express')

const {
    getData,
    changeData
} = require('../../dataProvider.js')

const isAuthed = require('../middleware/auth')

const router = express.Router()


router.get('/', isAuthed, async (req, res, next) => {
    const {
        login
    } = req.user

    let result = []

    try {
        const { bids } = await getData()
        result = bids.map(bid => {
            if (bid && bid.userLogin === login) {
                return bid;
            }
        });

        console.log(result)


    } catch (error) {
        res.json({
            success: false,
            message: error
        })
    }
    res.json({
        success: true,
        message: result
    })
})


router.get('/:id', async (req, res, next) => {
    const {
        id
    } = req.params

    result = null
    try {
        const { bids } = await getData()
        result = bids[id]
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
    }

    res.json({
        success: true,
        message: result
    })
})

router.get('/product/:productId', async (req, res, next) => {
    const {
        productId
    } = req.params
    console.log(productId)

    const result = []
    try {
        const { bids } = await getData()

        bids.forEach(bid => {
            if (bid && bid.productId === productId) {
                result.push(bid)
            }
        })
    } catch (error) {
        console.log(error)
        return res.json({
            success: false,
            message: error
        })
    }

    res.json({
        success: true,
        message: result
    })
})


router.post('/', isAuthed, async (req, res, next) => {
    const {
        productId,
        amount
    } = req.body

    let data
    let bidIsTooSmall = false
    try {
        data = await getData()
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
        return
    }
    const bidId = data.bids.length


    if (!Number(amount) || Number(amount) <= 0) return res.json({
        success: false,
        message: "Bid is not valid"
    })

    data.bids.forEach(bid => {
        if (bid && bid.productId == productId &&Number(bid.amount) > Number(amount)) {
           bidIsTooSmall = true
        }
    })

    if (bidIsTooSmall) return  res.json({
        success: false,
        message: "Bid is to small"
    })

    const bid = {
        bidId,
        productId : productId.toString(),
        userLogin: req.user.login,
        createdAt: new Date().getTime(),
        amount : amount.toString()
    }
    data.bids.push(bid)

    try {
        await changeData(data)
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
        return
    }
    res.json({
        success: true,
        message: bid
    })
})

router.delete('/', isAuthed, async (req, res, next) => {
    const {
        bidId
    } = req.body

    let data
    try {
        data = await getData()
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
        return
    }

    if (!data.bids[bidId]) {
        res.json({
            success: false,
            message: 'Bid not found'
        })
    }

    if (data.bids[bidId].userId !== req.user.login) {
        res.status(403).json({
            success: false,
            message: 'Bid is not yours'
        })
    }

    delete data.bids[bidId]

    try {
        await changeData(data)
    } catch (error) {
        res.json({
            success: false,
            message: error
        })
        return
    }
    res.send(200)
})

module.exports = router
