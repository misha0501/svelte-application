# Assignment 3 (Svelte frontend)
## How frontend and backend interact with each other?
In this assignment we are using "fetch" method to make http requests to the server. This method is built-in in browser JavaScript so we don't need to download it from external sources. 
If we are sending some JSON to the server we set header **Content-Type** to **application/json** so our server knows which type of data he receives.
If the route is for authorized users only, we send one more header **Authorization** with value **Bearer {token}**. We take token from local storage first and then write to Svelte store to store it while user serf the website.
## How authorization works?
After user send login/register request and it is successful, server send a response with encrypted token. We save it in local storage and write to Svelte store. Because svelte provides reactive variables, the data at the website changes immediately. User is redirect to the home page and now don't see login/register routes at the header. Now he see new tabs and able to make request's which only users can make. 
## What libraries do we use?
We tried to make the project lightweight so we used as few libraries as possible
### svelte-routing
We used this library to make a route system at the website. So when user click to the link, page, without refreshing, loads content from another component and showing it to a user. This is very common practice in modern websites
### query-string
Node has build-in library "querystring" but as we don't use node.js in browser javascript, we downloaded this library. We need it to parse the info from object into string so we can send a request to the server.
## How we divided the components?
First of all, we divided the components by "routing" criteria. Each page has it's own component so it makes life much more easier. Then, we added component for header as we use it everywhere in the website. 
Our main component called **App** is serving **Header** component and connects it with component from route. 
Also, we can split some page components to smaller ones. Like component for each bid at the homepage or component for search bar at the Home.