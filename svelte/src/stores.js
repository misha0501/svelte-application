import { writable } from 'svelte/store'

const tokenFromStorage = localStorage.getItem('token') || null
const adminFromStorage = localStorage.getItem('admin') === 'true' || false

export const token = writable(tokenFromStorage)
export const user = writable(null)
export const admin = writable(adminFromStorage)